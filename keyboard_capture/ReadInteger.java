package keyboard_capture;

import java.util.Scanner;

public class ReadInteger 
{
	public static void main(String[] args) 
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Hola �Qu� edad tienes? \n"
				+ "Escribe y presiona ENTER al terminar");
		int age = scan.nextInt();
			
		System.out.println("Genial, que buena edad: " + age);
	}
}
