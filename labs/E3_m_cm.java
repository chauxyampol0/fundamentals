package labs;

import java.util.Scanner;

/*Ingrese por teclado un valor en metros y el programa
 *  debe mostrar el resultado en centimetros*/

public class E3_m_cm 
{
	public static void main(String[] args)
	{        
        Scanner scan= new Scanner(System.in);
        
        System.out.println("Ingrese cantiad de metros");
        double metros = scan.nextDouble();
        
        //1m = 100 cm
        double centimetros = metros * 100;
        
        System.out.println(metros + " en centimetros es igual a: " + centimetros + " cm");
       
    }
}
