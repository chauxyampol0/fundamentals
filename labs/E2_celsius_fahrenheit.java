package labs;

import java.util.Scanner;

/*Ingrese por teclado una temperatura en Celsius y 
 *el programa debe mostrar el resultado en grados Fahrenheit*/

public class E2_celsius_fahrenheit 
{
	public static void main(String[] args)
	{        
        Scanner scan = new Scanner(System.in);
        
        System.out.println("ingrese temperatura en Celsius:");
        double c = scan.nextDouble();
        
        double f = (c * 1.8) + 32;
        
        System.out.println("La temperatura " + c + " �C  su conversi�n: " + f + " F");
    }
}
