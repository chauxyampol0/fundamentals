package vector;

public class Array
{
public static void main(String[] args) {
		
				
		double[] temperatures = new double[7];
		temperatures[0] = 31.3;
		temperatures[1] = 32;
		temperatures[2] = 33.7;
		temperatures[3] = 34;
		temperatures[4] = 33.1;
		
		System.out.println("Temperatura d�a 3: " + temperatures[2]);
		
		System.out.println("Tama�o del Array: " + temperatures.length);
		
		System.out.println("Usar un FOR para listar todo: ");
		
		for (int i=0; i<temperatures.length; i++){
			System.out.println("Temepratura dia " + (i+1) + " es: " + temperatures[i]);
		}		
	}
}
