package operators;

public class Logics {
	
	public static void main(String[] args) {
		
		int value1 = 1;
		int value2 = 2;
		
		boolean result1 = (value1 == 1) && (value2 == 2);
		System.out.println("value1 con 1 AND value2 con 2 --> resultado: " + result1);

		boolean result2 = (value1 == 1) || (value2 == 2);
		System.out.println("value1 con 1 OR value2 con 2 --> resultado: " + result2);
		
		System.out.println("___________________");
		boolean on = true;
		boolean off = false;
		System.out.println(on && off);
		System.out.println(on || off);
		System.out.println(on ^ off);
		System.out.println(!on || off);
	}
}
