package operators;

public class Relational 
{
public static void main(String[] args) {
		
		int value1 = 1;
		int value2 = 2;
		
		System.out.println("valor1 == valor2: " + 	(value1 == value2));
		System.out.println("valor1 != valor2: " + 	(value1 != value2));
		System.out.println("valor1 > valor2: " 	+ 	(value1 > value2));
		System.out.println("valor1 >= valor2: " + 	(value1 >= value2));
		System.out.println("valor1 < valor2: " 	+ 	(value1 < value2));
		System.out.println("valor1 <= valor2: " + 	(value1 <= value2));
	}
}
